import json
from funciones import *
with open("paises.json") as fichero:
    doc = json.load(fichero)
Fin_programa = False
while not Fin_programa:
    menu()
    opcion = pedir_opcion()
    if opcion == 1:
        opcion1(doc)
    elif opcion == 2:
        opcion2(doc)
    elif opcion == 3:
        opcion3(doc)
    elif opcion == 4:
        opcion4(doc)
    elif opcion == 5:
        opcion5(doc)
    elif opcion == 0:
        Fin_programa = True
    else:
        print("Introduce una opción correcta.")
print("Programa finalizado")