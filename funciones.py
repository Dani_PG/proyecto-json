def menu():
    print("")
    print("1. Listado de los nombres de los países con su respectiva capital.")
    print("2. Mostrar el número de países y el total de habitantes por continente.")
    print("3. Mostrar los países en los que se habla un idioma introducido por teclado.")
    print("4. Escribe un país y te dará su moneda, y mostrará dónde esa moneda es también oficial.")
    print("5. Escribe un país y dará los países que tienen fronteras con ese.")
    print("0. Salir")
    print("")

def pedir_opcion():
    opcion = int(input("Opción: "))
    while opcion < 0 or opcion > 5:
        opcion = int(input("Opción incorrecta: "))
    return opcion

def opcion1(doc):
    i = 0
    for paises in doc:
        print("País:",doc[i]['name'],"-- Capital:",doc[i]['capital'])
        i+=1

def opcion2(doc):
    i = 0
    num_paises_america = 0
    num_paises_asia = 0
    num_paises_africa = 0
    num_paises_europa = 0
    num_paises_oceania = 0
    num_paises_polar = 0
    habitantes_america = 0
    habitantes_asia = 0
    habitantes_africa = 0
    habitantes_europa = 0
    habitantes_oceania = 0
    habitantes_polar = 0
    for paises in doc:
        if doc[i]['region'] == "Americas":
            num_paises_america += 1
            habitantes_america += doc[i]['population']
        elif doc[i]['region'] == "Asia":
            num_paises_asia += 1
            habitantes_asia += doc[i]['population']
        elif doc[i]['region'] == "Africa":
            num_paises_africa +=1
            habitantes_africa += doc[i]['population']
        elif doc[i]['region'] == "Europe":
            num_paises_europa += 1
            habitantes_europa += doc[i]['population']
        elif doc[i]['region'] == "Oceania":
            num_paises_oceania += 1
            habitantes_oceania += doc[i]['population']
        else:
            num_paises_polar += 1
            habitantes_polar += doc[i]['population']
        i+=1
    print("")
    print("América:")
    print("Número de países:",num_paises_america,"-- Habitantes:",habitantes_america)
    print("")
    print("Asia:")
    print("Número de países:",num_paises_asia,"-- Habitantes:",habitantes_asia)
    print("")
    print("África:")
    print("Número de países:",num_paises_africa,"-- Habitantes:",habitantes_africa)
    print("")
    print("Europa:")
    print("Número de países:",num_paises_europa,"-- Habitantes:",habitantes_europa)
    print("")
    print("Oceanía:")
    print("Número de países:",num_paises_oceania,"-- Habitantes:",habitantes_oceania)
    print("")
    print("Antártida:")
    print("Número de países:",num_paises_polar,"-- Habitantes:",habitantes_polar)
    print("")

def opcion3(doc):
    i = 0
    print("Nota: Se debe escibir el nombre del idioma en inglés.")
    idioma = input("Escribe un idioma: ")
    for paises in doc:
        idiomas = doc[i]['languages']
        num_idiomas = len(idiomas)
        for j in range(0,num_idiomas):
            if idiomas[j]['name'] == idioma:
                print("En", doc[i]['name'],"se habla", idioma)
        i += 1

def opcion4(doc):
    print("Puedes consultar la lista de países en la opción número 1")
    i = 0
    posicion = False
    pais = input("Escribe un país: ")
    for paises in doc:
        if doc[i]['name'] == pais:
            posicion = True
            lista_monedas = doc[i]['currencies'][0]
            cont = 0
            for moneda in lista_monedas.values():
                if cont == 0:
                    marca_moneda = moneda
                    if posicion:
                        print("La moneda oficial de",pais,"es:",marca_moneda)
                        break
        i += 1
    if posicion:
        i = 0
        for paises in doc:
            moneda_repetida = doc[i]['currencies'][0]
            for moneda in moneda_repetida.values():
                if moneda == marca_moneda:
                    if pais != doc[i]['name']:
                        print(marca_moneda,"es tambíen moneda oficial en",doc[i]['name'])
            i += 1
    else:
        print("País no disponible en el fichero.")

def opcion5(doc):
    print("Puedes consultar la lista de países en la opción número 1")
    i = 0
    posicion = False
    pais = input("Escribe un país: ")
    for paises in doc:
        if doc[i]['name'] == pais:
            posicion = True
            frontera = doc[i]['borders']
            break
        i += 1
    i = 0
    if posicion:
        hasta = len(frontera)
        for paises in doc:
            for j in range (0,hasta):
                if doc[i]['alpha3Code'] == frontera[j]:
                    print(doc[i]['name'],"tiene frontera con",pais)
            i += 1
    else:
        print("País no disponible en el fichero.")