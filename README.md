# Proyecto JSON

Proyecto JSON de países

* 1. Listar información: Lista los nombres de los países con su respectiva capital.
* 2. Contar información: Mostrar el número de países y el total de habitantes por continente.
* 3. Buscar o filtrar información: Mostrar los países en los que se habla un idioma introducido por teclado.
* 4. Buscar información relacionada: Escribe un país, mostrará su moneda oficial y todos los países donde esa moneda también es oficial.
* 5. Ejercicio libre: Escribe un país y dará los países que tienen fronteras con ese.
